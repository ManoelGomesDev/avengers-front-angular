import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CaptianAmericanComponent } from './components/captian-american/captian-american.component';
import { IronmanComponent } from './components/ironman/ironman.component';


const routes: Routes = [
  { path: '', component: IronmanComponent, pathMatch: 'full' },
  { path: 'captianamerican', component: CaptianAmericanComponent },
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
